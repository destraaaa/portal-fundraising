<!DOCTYPE html>
<html>
	<head>
		<title>New Item</title>
	</head>
	<body>
		<form method="post" action="{{URL::to('/items/store')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="text" name="name" id="name" placeholder="name">
			<input type="text" name="target" id="trget" placeholder="target">
			<input type="text" name="img_src" id="img_src" placeholder="image source">
			<input type="text" name="description" id="description" placeholder="description">
			<input type="text" name="id_faculty" id="id_faculty" placeholder="id_faculty">
			<button type="submit">Add</button>
		</form>
	</body>
</html>