<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
	</head>
	<body>
		<form method="post" action="{{URL::to('/auth/login/check')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="text" name="email" id="email" placeholder="email">
			<input type="password" name="password" id="password" placeholder="password">
			<button type="submit">Login</button>
		</form>
	</body>
</html>