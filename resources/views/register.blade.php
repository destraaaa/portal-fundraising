<!DOCTYPE html>
<html>
	<head>
		<title>Register</title>
	</head>
	<body>
		<form method="post" action="{{URL::to('/auth/register/check')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="text" name="name" id="name" placeholder="name">
			<input type="text" name="email" id="email" placeholder="email">
			<input type="password" name="password" id="password" placeholder="password">
			<button type="submit">Register</button>
		</form>
	</body>
</html>