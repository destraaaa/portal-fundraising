<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model {

	protected $table = 'packages';

	protected $fillable = [
		'name',
		'price',
		'description',
		'img_src',
		'id_reward',
		'id_item',
		'sold'
	];

}
