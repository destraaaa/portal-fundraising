<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model {

	protected $table = 'alumnis';

	protected $fillable = [
		'name',
		'email',
		'password'
	];

	protected $hidden = ['password'];

}
