<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';

	protected $fillable = [
		'name',
		'target',
		'achieved',
		'img_src',
		'description',
		'id_major'
	];

}
