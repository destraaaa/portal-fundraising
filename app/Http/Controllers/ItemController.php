<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Faculty;
use App\Item;

use Input;
use Redirect;
class ItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('new-item');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = Input::get("name");
		$target = Input::get("target");
		$achieved = 0;
		$img_src = Input::get("img_src");
		$description = Input::get("description");
		$id_faculty = Input::get("id_faculty");
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		$item = Item::create(['name' => $name,
						  'target' => $target,
						  'achieved' => $achieved,
						  'img_src' => $img_src,
						  'description' => $description,
						  'id_faculty' => $id_faculty,
						  'created_at' => $created,
						  'updated_at' => $updated]);
		return Redirect::intended('/');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = 0)
	{
		$faculties = Faculty::all();
		$items = Item::whereRaw('id_faculty = ?',[$id])->get();
		return view('empty', compact('faculties', 'items'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
