<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Faculty;
use App\Item;
use App\Alumni;

use Input;
use Hash;
use Session;
use Redirect;

class SiteController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$faculties = Faculty::all();
		$items = Item::all();
		if(Session::has('loggedin')){
			$loggedin = Session::get('loggedin');
		}
		return view('empty', compact('faculties', 'items', 'loggedin'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function login(){
		$faculties = Faculty::all();
		return view('login', compact('faculties'));
	}

	public function logout(){
		Session::forget('loggedin');
		Session::put('success_message', "Anda telah signout");
		return Redirect::intended('/');
	}

	public function check(){
		$email = Input::get("email");
		$password = Input::get("password");
		$users = Alumni::whereRaw('email = ?', [$email])->first();
		if($users){
			if (Hash::check($password, $users->password))
			{
			    Session::put('loggedin', $users);
				Session::put('success_message', "Anda berhasil masuk.");
				return Redirect::intended('/');
			}
			else{
				Session::put('error_message', "Email atau password salah.");
				return Redirect::intended('/');
			}
		}
	}
}
