<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Alumni;

use Input;
use Hash;
use Session;
use Redirect;

class AlumniController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('register');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = Input::get("name");
		$email = Input::get("email");
		$password = Input::get("password");
		$created = date("Y-m-d H:i:s");
		$updated = $created;
		Session::put('success_message', "Pendaftaran Berhasil. Silakan verifikasi email.");
		$users = Alumni::create(['name' => $name,
						  'email' => $email,
						  'password' => Hash::make($password),
						  'created_at' => $created,
						  'updated_at' => $updated]);
		Session::put('loggedin', $users);
		Session::put('success_message', "Anda berhasil masuk.");
		return Redirect::intended('/');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
