<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::get('/', 'SiteController@index');
Route::get('/auth/login/show', 'SiteController@login');
Route::get('/auth/register/show', 'AlumniController@create');
Route::get('/auth/logout', 'SiteController@logout');
Route::post('/auth/login/check', 'SiteController@check');
Route::post('/auth/register/check', 'AlumniController@store');
Route::get('/items/show/{id_faculty?}', 'ItemController@show');
Route::get('/items/create', 'ItemController@create');
Route::post('/items/store', 'ItemController@store');