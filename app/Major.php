<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model {

	protected $table = 'models';

	protected $fillable = [
		'name',
		'description',
		'id_faculty'
	];

}
