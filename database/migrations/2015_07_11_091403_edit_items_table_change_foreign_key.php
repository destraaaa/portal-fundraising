<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditItemsTableChangeForeignKey extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('items', function($table)
		{
		    $table->dropForeign('items_id_faculty_foreign');
		    $table->dropColumn('id_faculty');
		    $table->integer('id_major')->unsigned();
			$table->foreign('id_major')
			      ->references('id')->on('majors')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items', function($table)
		{
		    $table->dropForeign('items_id_major_foreign');
			$table->dropColumn('id_major');
			$table->integer('id_faculty')->unsigned();
			$table->foreign('id_faculty')
			      ->references('id')->on('faculties')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
		});
	}

}
