<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('packages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('price');
			$table->string('img_src');
			$table->text('description');
			$table->integer('id_reward')->unsigned();
			$table->foreign('id_reward')
			      ->references('id')->on('rewards')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
			$table->integer('id_item')->unsigned();
			$table->foreign('id_item')
			      ->references('id')->on('items')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
			$table->boolean('sold');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('packages');
	}

}
