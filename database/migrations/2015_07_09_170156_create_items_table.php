<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('target');
			$table->integer('achieved');
			$table->string('img_src');
			$table->text('description');
			$table->integer('id_faculty')->unsigned();
			$table->foreign('id_faculty')
			      ->references('id')->on('faculties')
			      ->onDelete('cascade')
			      ->onUpdate('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
