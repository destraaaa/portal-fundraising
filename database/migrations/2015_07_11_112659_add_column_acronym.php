<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAcronym extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faculties', function($table)
		{
		    $table->string('acronym');
		});

		Schema::table('majors', function($table)
		{
		    $table->string('acronym');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faculties', function($table)
		{
		    $table->dropColumn('acronym');
		});

		Schema::table('majors', function($table)
		{
		    $table->dropColumn('acronym');
		});
	}
}
